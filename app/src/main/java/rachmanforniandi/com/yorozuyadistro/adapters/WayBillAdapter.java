package rachmanforniandi.com.yorozuyadistro.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.waybills.WayBill;

public class WayBillAdapter extends RecyclerView.Adapter<WayBillAdapter.WayBillHolder>  {

    private List<WayBill.RajaOngkir.Result.Manifest> manifests;
    Context context;

    public WayBillAdapter(Context context,List<WayBill.RajaOngkir.Result.Manifest> manifests ) {
        this.manifests = manifests;
        this.context = context;
    }

    @NonNull
    @Override
    public WayBillHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_waybill,parent,false);
        WayBillHolder wayBillHolder = new WayBillHolder(view);
        return wayBillHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WayBillAdapter.WayBillHolder holder, int position) {
        WayBill.RajaOngkir.Result.Manifest waybill = manifests.get(position);
        holder.txtDescription.setText(waybill.getManifest_description());
        holder.txtDateTime.setText(waybill.getManifest_date()+ "_" + waybill.getManifest_time());

    }

    @Override
    public int getItemCount() {
        return manifests.size();
    }

    public class WayBillHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_description) TextView txtDescription;
        @BindView(R.id.txt_date_time) TextView txtDateTime;
        //@BindView(R.id.img_time_line) ImageView imgTimeLine;


        public WayBillHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

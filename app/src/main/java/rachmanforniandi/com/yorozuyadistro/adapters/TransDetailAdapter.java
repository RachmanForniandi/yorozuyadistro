package rachmanforniandi.com.yorozuyadistro.adapters;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransDetail;

public class TransDetailAdapter  extends RecyclerView.Adapter<TransDetailAdapter.TransDetailHolder> {

    private List<TransDetail.Data.DetailTransaction> transactions;

    Context context;

    public TransDetailAdapter(Context context, List<TransDetail.Data.DetailTransaction> transactions) {
        this.context = context;
        this.transactions = transactions;
    }

    @Override
    public TransDetailAdapter.TransDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail_transaction, parent, false);
        TransDetailAdapter.TransDetailHolder transDetailHolder = new TransDetailAdapter.TransDetailHolder(view);
        return transDetailHolder;
    }

    @Override
    public void onBindViewHolder(TransDetailAdapter.TransDetailHolder holder, final int position) {
        final TransDetail.Data.DetailTransaction detailTransaction = transactions.get(position);

        holder.tvNameItem.setText(detailTransaction.getProduct());
        holder.tvDescriptionItem.setText("Harga: "+ detailTransaction.getPrice() +
                "Jumlah: " +detailTransaction.getQty());
        holder.tvTotalPrice.setText("Total Harga: "+ detailTransaction.getTotal());

    }

    public class TransDetailHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name_item)
        TextView tvNameItem;
        @BindView(R.id.tv_description_item)
        TextView tvDescriptionItem;
        @BindView(R.id.tv_total_price)
        TextView tvTotalPrice;



        public TransDetailHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }
}
package rachmanforniandi.com.yorozuyadistro.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransUser;
import rachmanforniandi.com.yorozuyadistro.views.activities.TransDetailActivity;
import rachmanforniandi.com.yorozuyadistro.views.activities.WayBillActivity;

public class TransPaidAdapter extends RecyclerView.Adapter<TransPaidAdapter.TransPaidHolder>  {

    private List<TransUser.Data> transPaid;
    Context context;

    public TransPaidAdapter(Context context,List<TransUser.Data> transPaid) {
        this.transPaid = transPaid;
        this.context = context;
    }

    @NonNull
    @Override
    public TransPaidHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction,parent,false);
        TransPaidHolder transPaidHolder = new TransPaidHolder(view);
        return transPaidHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TransPaidHolder holder, int position) {
        final TransUser.Data data = transPaid.get(position);

        if (data.getStatus_transaction().equals("sent")) {
            holder.btnActionTransaction.setText("Lacak Pengiriman");
            holder.btnActionTransaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, WayBillActivity.class);
                    intent.putExtra("WAYBILL",data.getResi_code());
                    context.startActivity(intent);
                }
            });
        }else {
            holder.btnActionTransaction.setVisibility(View.GONE);
        }
        holder.txtTitleTransaction.setText(data.getTransaction_code());
        holder.txtStatusTransaction.setText(data.getStatus_transaction());
        holder.dataTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TransDetailActivity.class);
                intent.putExtra("TRANSACTION_CODE",data.getTransaction_code());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return transPaid.size();
    }

    public class TransPaidHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_title_transaction) TextView txtTitleTransaction;
        @BindView(R.id.txt_status_transaction)TextView txtStatusTransaction;
        @BindView(R.id.btn_action_transaction) Button btnActionTransaction;
        @BindView(R.id.data_transaction) CardView dataTransaction;

        public TransPaidHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}

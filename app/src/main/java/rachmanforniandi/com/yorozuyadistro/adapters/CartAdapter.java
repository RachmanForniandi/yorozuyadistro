package rachmanforniandi.com.yorozuyadistro.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.cart.Cart;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.GlideUtils;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.TheConverter;
import rachmanforniandi.com.yorozuyadistro.views.activities.ItemCartActivity;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartHolder> {
    private Context context;
    private List<Cart> carts;

    public CartAdapter(Context context, List<Cart> carts) {
        this.context = context;
        this.carts = carts;
    }


    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart,parent,false);
        return  new CartHolder(view);
    }

    @Override
    public void onBindViewHolder(final CartHolder holder, final int position) {
        final Cart cart = carts.get(position);
        cart.setQuantity(1);
        cart.setTotal(cart.getTotal());

        GlideUtils.displayImages(context, cart.getImage(), holder.imgProduct);
        holder.tvNameProduct.setText(cart.getProduct());
        holder.tvPrice.setText(
                TheConverter.rupiah(cart.getPrice())
        );

        holder.spinnerQty.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int total = Integer.valueOf(parent.getItemAtPosition(position).toString())* cart.getPrice();
                holder.txtTotalPrice.setText(TheConverter.rupiah(total));

                cart.setQuantity(Integer.valueOf(parent.getItemAtPosition(position).toString()));
                cart.setTotal(total);

                getPriceTotal();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.btnDeleteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);

                builder.setTitle("Konfirmasi");
                builder.setMessage("Yakin ingin menghapus " + cart.getProduct() + " dari keranjang?");

                builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Toast.makeText(context, "Hapus", Toast.LENGTH_SHORT).show();
                        App.sqLiteDBHelper.removeItem(String.valueOf(cart.getId_cart()));
                        carts.remove(position);
                        notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       dialog.dismiss();

                    }
                });
                builder.show();
            }
        });
    }

    public class CartHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_product) ImageView imgProduct;
        @BindView(R.id.tv_name_product)TextView tvNameProduct;
        @BindView(R.id.tv_price) TextView tvPrice;
        @BindView(R.id.btn_delete_item) ImageButton btnDeleteItem;
        @BindView(R.id.spinner_qty) Spinner spinnerQty;
        @BindView(R.id.txt_total_price)TextView txtTotalPrice;

        public CartHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            ArrayList<String> valueList = new ArrayList<>();
            for (int i = 1; i <=100; i++){
                valueList.add(String.valueOf(i));
            }

            ArrayAdapter<String> valueAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, valueList);
            spinnerQty.setAdapter(valueAdapter);
        }
    }

    public int getPriceTotal(){
        int priceTotal =0;
        for (int i= 0; i < carts.size();i ++){
            priceTotal += carts.get(i).getTotal();
        }
        ItemCartActivity.tvPriceTotal.setText("" + TheConverter.rupiah(priceTotal));

        return priceTotal;
    }


    @Override
    public int getItemCount() {
        return carts.size();
    }


}

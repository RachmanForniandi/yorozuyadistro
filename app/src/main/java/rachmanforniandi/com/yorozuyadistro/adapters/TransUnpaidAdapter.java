package rachmanforniandi.com.yorozuyadistro.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransUser;
import rachmanforniandi.com.yorozuyadistro.views.activities.TransDetailActivity;
import rachmanforniandi.com.yorozuyadistro.views.activities.UploadReceiptActivity;

public class TransUnpaidAdapter extends RecyclerView.Adapter<TransUnpaidAdapter.TransUnpaidHolder> {

    private List<TransUser.Data> transUnpaid;

    Context context;

    public TransUnpaidAdapter(Context context,List<TransUser.Data> transaction){
        this.context = context;
        this.transUnpaid = transaction;
    }

    @Override
    public TransUnpaidAdapter.TransUnpaidHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction,parent,false);
        TransUnpaidHolder transUnpaidHolder = new TransUnpaidHolder(view);
        return transUnpaidHolder;
    }

    @Override
    public void onBindViewHolder(TransUnpaidAdapter.TransUnpaidHolder holder, final int position) {
        final TransUser.Data data = transUnpaid.get(position);

        if (data.getStatus_transaction().equals("pending")){
            holder.btnActionTransaction.setVisibility(View.GONE);
        }else {
            holder.btnActionTransaction.setText("Upload bukti transfer");
            holder.btnActionTransaction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, UploadReceiptActivity.class);
                    intent.putExtra("TRANSACTION_CODE",data.getTransaction_code());
                    context.startActivity(intent);
                }
            });
        }
        holder.txtTitleTransaction.setText(data.getTransaction_code());
        holder.txtStatusTransaction.setText(data.getStatus_transaction());
        holder.dataTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, TransDetailActivity.class);
                intent.putExtra("TRANSACTION_CODE",data.getTransaction_code());
                context.startActivity(intent);
            }
        });

    }

    public class TransUnpaidHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_title_transaction) TextView txtTitleTransaction;
        @BindView(R.id.txt_status_transaction)TextView txtStatusTransaction;
        @BindView(R.id.btn_action_transaction) Button btnActionTransaction;
        @BindView(R.id.data_transaction) CardView dataTransaction;


        public TransUnpaidHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @Override
    public int getItemCount() {
        return transUnpaid.size();
    }
}

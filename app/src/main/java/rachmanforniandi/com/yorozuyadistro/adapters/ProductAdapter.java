package rachmanforniandi.com.yorozuyadistro.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.products.Product;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.TheConverter;
import rachmanforniandi.com.yorozuyadistro.views.activities.DetailActivity;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductHolder> {

    private List<Product.Data> products;

    Context context;

    public ProductAdapter(Context context,List<Product.Data> item){
        this.context = context;
        this.products = item;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item,parent,false);
        ProductHolder productHolder = new ProductHolder(view);
        return productHolder;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int position) {
        holder.productName.setText(products.get(position).getProduct());


        int price = products.get(position).getPrice();
        holder.productPrice.setText("IDR " + TheConverter.rupiah(price));


        RequestOptions imgState = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.thumbnail_01)
                .error(R.drawable.img_broken)
                .dontAnimate()
                .priority(Priority.HIGH);

        Glide.with(context)
                .load(products.get(position).getImage())
                .apply(imgState)
                .into(holder.imgProduct);
        Log.e("debugImgData",products.get(position).getImage());

        holder.imgProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toDetail = new Intent(context, DetailActivity.class);
                toDetail.putExtra("ID_PRODUCT", products.get(position).getId());
                toDetail.putExtra("PRODUCT_IMAGE", products.get(position).getImage());
                context.startActivity(toDetail);
            }
        });
    }

    public class ProductHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_product)ImageView imgProduct;
        @BindView(R.id.product_name)TextView productName;
        @BindView(R.id.product_price)TextView productPrice;

        public ProductHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }
}

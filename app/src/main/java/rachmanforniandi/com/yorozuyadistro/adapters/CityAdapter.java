package rachmanforniandi.com.yorozuyadistro.adapters;

import android.app.Activity;
import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.RajaOngkirKey;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.cities.City;

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityHolder> implements Filterable {

    private List<City.RajaOngkir.Results> results;
    private List<City.RajaOngkir.Results> resultsFiltered;

    Context context;

    public CityAdapter(Context context, List<City.RajaOngkir.Results> item) {
        this.context = context;
        this.results = item;
        this.resultsFiltered = item;
    }

    @Override
    public CityAdapter.CityHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        CityAdapter.CityHolder productHolder = new CityAdapter.CityHolder(view);
        return productHolder;
    }

    @Override
    public void onBindViewHolder(CityAdapter.CityHolder holder, final int position) {
        final City.RajaOngkir.Results city = resultsFiltered.get(position);
        holder.tvCityName.setText(city.getCity_name());
        holder.tvCityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RajaOngkirKey.DESTINATION = city.getCity_id();
                RajaOngkirKey.DESTINATION_NAME = city.getCity_name();

                ((Activity)context).finish();
            }
        });

    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if (charString.isEmpty()){
                    resultsFiltered = results;
                }else {
                    List<City.RajaOngkir.Results> resultsList = new ArrayList<>();
                    for (City.RajaOngkir.Results row : results){
                        if (row.getCity_name().toLowerCase().contains(charString.toLowerCase())){
                            resultsList.add(row);
                        }
                    }
                    resultsFiltered = resultsList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = resultsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                resultsFiltered = (ArrayList<City.RajaOngkir.Results>)results.values;
                notifyDataSetChanged();
            }
        };
    }

    public class CityHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_city_name)
        TextView tvCityName;

        public CityHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public int getItemCount() {
        return resultsFiltered.size();
    }
}

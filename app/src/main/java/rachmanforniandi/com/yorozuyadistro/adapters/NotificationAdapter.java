package rachmanforniandi.com.yorozuyadistro.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.notifications.Notification;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {


    private List<Notification.Data> notificationPaid;
    Context context;


    public NotificationAdapter(Context context, List<Notification.Data> notificationPaid) {
        this.notificationPaid = notificationPaid;
        this.context = context;
    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,parent,false);
        NotificationHolder notificationHolder = new NotificationHolder(view);
        return notificationHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder holder, int position) {
        Notification.Data notify = notificationPaid.get(position);
        holder.txtMessage.setText(notify.getDescription());
        holder.txtDate.setText(notify.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return notificationPaid.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_msg) TextView txtMessage;
        @BindView(R.id.txt_date) TextView txtDate;

        public NotificationHolder(View view) {

            super(view);
            ButterKnife.bind(this,view);
        }
    }


}

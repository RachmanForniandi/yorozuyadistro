package rachmanforniandi.com.yorozuyadistro.supportUtilities;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;

import rachmanforniandi.com.yorozuyadistro.R;

public class GlideUtils {

    public static void displayImages(Context context, String urlImage, ImageView imageView){
        RequestOptions imgState = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.img_thumbnail)
                .error(R.drawable.img_broken)
                .priority(Priority.HIGH);

        Glide.with(context)
                .load(urlImage)
                .apply(imgState)
                .into(imageView);
    }
}

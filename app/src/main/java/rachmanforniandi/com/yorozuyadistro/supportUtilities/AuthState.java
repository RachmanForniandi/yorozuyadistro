package rachmanforniandi.com.yorozuyadistro.supportUtilities;

import android.content.Context;
import android.view.Menu;
import android.widget.Toast;

import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.users.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthState {
    public static void isLoggedIn(Menu menu){
        menu.findItem(R.id.nav_notif).setVisible(true);
        menu.findItem(R.id.nav_transaksi).setVisible(true);
        menu.findItem(R.id.nav_profile).setVisible(true);
        menu.findItem(R.id.nav_logout).setVisible(true);

        menu.findItem(R.id.nav_login).setVisible(false);
    }

    public static void isLoggedOut(Menu menu){
        menu.findItem(R.id.nav_notif).setVisible(false);
        menu.findItem(R.id.nav_transaksi).setVisible(false);
        menu.findItem(R.id.nav_profile).setVisible(false);
        menu.findItem(R.id.nav_logout).setVisible(false);

        menu.findItem(R.id.nav_login).setVisible(true);
    }

    public static void updateToken(Context context, String token){
        App.sessionPref = App.sharedPrefManager.getUserDetails();

        ApiCommand apiCommand = APIProcessing.getLinkUrl().create((ApiCommand.class));
        Call<User> call = apiCommand.updateTokenFireBase(App.sessionPref.get(SharedPrefManager.SESSION_ID), token);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    Toast.makeText(context, "Token firebase berhasil diperbarui", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(context,response.message(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }


}

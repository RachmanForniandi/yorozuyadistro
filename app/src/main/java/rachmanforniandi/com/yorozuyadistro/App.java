package rachmanforniandi.com.yorozuyadistro;

import android.app.Application;
import android.util.Log;

import com.google.android.gms.common.internal.service.Common;

import java.util.HashMap;

import rachmanforniandi.com.yorozuyadistro.sourceData.database.SQLiteDBHelper;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;

public class App extends Application {

    public static SharedPrefManager sharedPrefManager;
    public static SQLiteDBHelper sqLiteDBHelper;
    public static HashMap<String,String> sessionPref;

    public static boolean isNetworkConnected;
    @Override
    public void onCreate() {
        super.onCreate();

        sqLiteDBHelper = new SQLiteDBHelper(this);

        sharedPrefManager = new SharedPrefManager(this);

        sessionPref = sharedPrefManager.getUserDetails();

        Log.e("_logBase","testing");
        //isNetworkConnected = Commons.isNetworkConnected(this);
    }
}

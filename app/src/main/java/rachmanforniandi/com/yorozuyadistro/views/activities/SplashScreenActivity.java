package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import rachmanforniandi.com.yorozuyadistro.R;

public class SplashScreenActivity extends AppCompatActivity {

    private final int DURATION = 3000; //1000= 1 detik

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent toNextPage = new Intent(SplashScreenActivity.this,MainActivity.class);
                startActivity(toNextPage);
                finish();
            }
        },DURATION);
    }
}

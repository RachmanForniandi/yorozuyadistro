package rachmanforniandi.com.yorozuyadistro.views.activities;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.TabAdapter;
import rachmanforniandi.com.yorozuyadistro.views.fragments.LoginFragment;
import rachmanforniandi.com.yorozuyadistro.views.fragments.RegisterFragment;

public class RegisterActivity extends AppCompatActivity {

    public static TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        tabLayout = findViewById(R.id.tabLayout);

        ButterKnife.bind(this);

        newTab(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        getSupportActionBar().setTitle("Pengguna Baru");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void newTab(ViewPager viewPager){
        TabAdapter adapter = new TabAdapter(getSupportFragmentManager());
        adapter.addFragment(new RegisterFragment(),"Daftar");
        adapter.addFragment(new LoginFragment(),"Masuk");
        viewPager.setAdapter(adapter);
    }
}

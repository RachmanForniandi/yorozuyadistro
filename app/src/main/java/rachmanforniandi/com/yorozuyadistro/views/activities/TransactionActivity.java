package rachmanforniandi.com.yorozuyadistro.views.activities;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.TabAdapter;
import rachmanforniandi.com.yorozuyadistro.views.fragments.transaction.PaidFragment;
import rachmanforniandi.com.yorozuyadistro.views.fragments.transaction.UnpaidFragment;

public class TransactionActivity extends AppCompatActivity {
    //private TabLayout tabTransaction;
    @BindView(R.id.view_transaction) ViewPager viewTransaction;
    @BindView(R.id.tab_transaction)TabLayout tabTransaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        ButterKnife.bind(this);
        tabTransaction = findViewById(R.id.tab_transaction);

        addTabTransaction(viewTransaction);
        tabTransaction.setupWithViewPager(viewTransaction);

        getSupportActionBar().setTitle("Transaksi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void addTabTransaction(ViewPager viewPager){
        TabAdapter tabAdapter = new TabAdapter(getSupportFragmentManager());
        tabAdapter.addFragment(new UnpaidFragment(),"Tertunda");
        tabAdapter.addFragment(new PaidFragment(),"Proses");
        viewPager.setAdapter(tabAdapter);
    }


}

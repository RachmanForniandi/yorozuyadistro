package rachmanforniandi.com.yorozuyadistro.views.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xwray.passwordview.PasswordView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.users.User;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.TheConverter;
import rachmanforniandi.com.yorozuyadistro.views.activities.RegisterActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    @BindView(R.id.input_name)
    EditText inputName;

    @BindView(R.id.input_email)
    EditText inputEmail;

    @BindView(R.id.input_password)
    PasswordView inputPassword;

    @BindView(R.id.input_re_password)
    PasswordView inputRePassword;

    @BindView(R.id.btn_register)
    Button btnRegister;



    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);

        ButterKnife.bind(this,view);
        return view;

    }

    @OnClick(R.id.btn_register)
    public void onClick(View view){
        if (inputName.length() > 0 && inputEmail.length() > 0 && inputPassword.length() > 0 && inputRePassword.length() >0){
            if (!inputPassword.getText().toString().equals(inputRePassword.getText().toString())){
                Toast.makeText(getContext(), "Konfirmasi password dengan benar", Toast.LENGTH_SHORT).show();
            } else if (!TheConverter.isValidEmailId(inputEmail.getText().toString())) {
                Toast.makeText(getContext(), "Isi format email dengan benar", Toast.LENGTH_SHORT).show();
            }else{
                AuthRegisterNewUser();
            }
        }else {
            Toast.makeText(getContext(), "Isi data dengan benar", Toast.LENGTH_SHORT).show();
        }
    }

    private void AuthRegisterNewUser() {
        ApiCommand apiCommand = APIProcessing.getLinkUrl().create((ApiCommand.class));
        Call<User> call = apiCommand.authRegister(inputName.getText().toString(),
                inputEmail.getText().toString(), inputPassword.getText().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    Toast.makeText(getContext(),"Pendaftaran berhasil",Toast.LENGTH_SHORT).show();
                    RegisterActivity.tabLayout.getTabAt(1).select();
                }else {
                    Toast.makeText(getContext(),response.message(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });

    }
}

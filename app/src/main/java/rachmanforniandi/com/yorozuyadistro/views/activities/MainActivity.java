package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.ProductAdapter;
import rachmanforniandi.com.yorozuyadistro.dialog.LoginDialog;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.AuthState;
import retrofit2.Call;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.List;

import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.products.Product;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.search_view) MaterialSearchView searchView;
    @BindView(R.id.fab)FloatingActionButton fab;
    @BindView(R.id.drawer_layout)DrawerLayout drawer;
    @BindView(R.id.nav_view)NavigationView navigationView;
    @BindView(R.id.list_item_product)RecyclerView listItemProduct;
    @BindView(R.id.swipe_refresh_data)SwipeRefreshLayout swipeRefreshData;
    ProductAdapter productAdapter;
    Menu menu;
    //List<Product> product = new ArrayList<>();


    private void getProductData(){

        swipeRefreshData.setRefreshing(true);
        ApiCommand apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);
        Call<Product> call = apiCommand.getProducts();
        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(Call<Product> call, Response<Product> response) {
                Log.e("_logResponse1st",response.toString());
                        Product product = response.body();
                //Log.e("_logSize", String.valueOf(product.products.size()));

                List<Product.Data> products = product.getProducts();
                //Log.e("_logSize", String.valueOf(products.size()));
                //System.out.println("logSizeOfProducts: "+String.valueOf(products.size()));

                listItemProduct.setAdapter(new ProductAdapter(MainActivity.this,products));
                /*listItemProduct.setHasFixedSize(true);
                (new ProductAdapter(MainActivity.this,products)).notifyDataSetChanged();*/


                for (int i=0; i < products.size(); i++){
                    Log.e("_logNameProducts", products.get(i).getProduct());
                }

                swipeRefreshData.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<Product> call, Throwable t) {
                swipeRefreshData.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar =findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        ButterKnife.bind(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        App.sessionPref = App.sharedPrefManager.getUserDetails();
        Log.d("token_firebase", ""+App.sessionPref.get(SharedPrefManager.SESSION_TOKEN));
        //productAdapter = new ProductAdapter()
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Toast.makeText(MainActivity.this, query, Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        navigationView.setNavigationItemSelectedListener(this);
        menu = navigationView.getMenu();

        listItemProduct.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));


        swipeRefreshData.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listItemProduct.setAdapter(null);
                getProductData();
            }
        });

        getProductData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProductData();

        if (menu != null){
            if (App.sharedPrefManager.isLoggedIn()){
                AuthState.isLoggedIn(menu);
            }else {
                AuthState.isLoggedOut(menu);
            }
        }
    }

    @OnClick(R.id.fab)
    public void onClick(View view){
        switch (view.getId()){
            case R.id.fab:
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_menu, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            /*Toast.makeText(getApplicationContext(),"Cart",Toast.LENGTH_SHORT).show();
            return true;*/
            if (App.sharedPrefManager.isLoggedIn()){
                startActivity(new Intent(MainActivity.this,ItemCartActivity.class));
            }else {
                new LoginDialog().showLoginDialog(MainActivity.this,menu);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_login) {
            startActivity(new Intent(MainActivity.this,RegisterActivity.class));
        }else if (id == R.id.nav_notif) {
            startActivity(new Intent(MainActivity.this,NotifActivity.class));
        } else if (id == R.id.nav_transaksi) {
            startActivity(new Intent(MainActivity.this,TransactionActivity.class));
        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(MainActivity.this,ProfileActivity.class));
        } else if (id == R.id.nav_logout) {

            AuthState.updateToken(MainActivity.this,"");

            App.sharedPrefManager.userLogout();
            AuthState.isLoggedOut(menu);
        }
        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

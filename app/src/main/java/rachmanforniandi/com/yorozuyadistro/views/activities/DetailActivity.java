package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.glide.slider.library.Animations.DescriptionAnimation;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.RajaOngkirKey;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.dialog.CartDialog;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.detail.Detail;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.TheConverter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener {

    Bundle bundle;
    @BindView(R.id.tv_name_item_detail)TextView nameItem;
    @BindView(R.id.tv_price_item_detail)TextView priceItem;
    @BindView(R.id.tv_description_item_detail)TextView descriptionItem;
    @BindView(R.id.btn_add_cart_item)ImageButton btnAddCartItem;
    @BindView(R.id.btn_checkout)Button btnCheckOut;
    @BindView(R.id.slider)SliderLayout slideLayout;

    int detailPrice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        bundle = getIntent().getExtras();
        Log.e("_logImgIntent", bundle.getString("PRODUCT_IMAGE"));

        getDetailProducts();

        getSupportActionBar().setTitle("Detail Barang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @OnClick({R.id.btn_add_cart_item,R.id.btn_checkout})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_add_cart_item:

                addToCart();
                new CartDialog().showCartDialog(DetailActivity.this);
                break;

            case R.id.btn_checkout:
                addToCart();
                RajaOngkirKey.PURCHASE_NOW = true;
                startActivity(new Intent(DetailActivity.this,ItemCartActivity.class));
                finish();
                break;
        }
    }

    private void addToCart() {
        if (App.sqLiteDBHelper.checkExists(bundle.getInt("ID_PRODUCT"))==0){

            Long id_cart= App.sqLiteDBHelper.addToCart(bundle.getInt("ID_PRODUCT"),
                    nameItem.getText().toString(),
                    bundle.getString("PRODUCT_IMAGE"),detailPrice);
            Log.e("_logCartId", String.valueOf(id_cart));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void getDetailProducts() {
        ApiCommand apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);
        Call<Detail> call = apiCommand.getDetail(bundle.getInt("ID_PRODUCT"));
        call.enqueue(new Callback<Detail>() {
            @Override
            public void onResponse(Call<Detail> call, Response<Detail> response) {
                Log.e("_logResponse", response.toString() );

                Detail.Data data = response.body().getData();

                nameItem.setText(data.getProduct());
                detailPrice = data.getPrice();
                priceItem.setText("" + TheConverter.rupiah(data.getPrice()));

                if (data.getDescription() != null){
                    descriptionItem.setText(data.getDescription());
                }

                Detail detail = response.body();
                List<Detail.Data.Images> images = detail.getData().getImages();

                ArrayList<String> arrayList = new ArrayList<>();
                for (Detail.Data.Images img : images){
                    arrayList.add(img.getImage());
                    Log.e("_logImages",img.getImage());
                    Log.e("_logImages2",""+detail.getData().getImages());

                }
                setSlider(arrayList);

            }

            @Override
            public void onFailure(Call<Detail> call, Throwable t) {

            }
        });
    }

    private void setSlider(ArrayList<String> urlImg) {
        //ButterKnife.bind(this);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.centerCrop();
        //.diskCacheStrategy(DiskCacheStrategy.NONE)
        requestOptions.placeholder(R.drawable.thumbnail_01);
        requestOptions.error(R.drawable.img_broken);

        for (int i = 0; i < urlImg.size(); i++) {
            DefaultSliderView sliderView = new DefaultSliderView(this);
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(urlImg.get(i))
                    .setRequestOption(requestOptions)
                    .setBackgroundColor(Color.TRANSPARENT)
                    .setProgressBarVisible(false)
                    .setOnSliderClickListener(this);

            //add your extra information
            sliderView.bundle(new Bundle());
            slideLayout.addSlider(sliderView);

        }
        // set Slider Transition Animation
        // mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Default);
        slideLayout.setPresetTransformer(SliderLayout.Transformer.Accordion);

        slideLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        slideLayout.setCustomAnimation(new DescriptionAnimation());
        slideLayout.setDuration(4000);
        slideLayout.addOnPageChangeListener(this);
    }

    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}

package rachmanforniandi.com.yorozuyadistro.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.NotificationAdapter;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.notifications.Notification;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotifActivity extends AppCompatActivity {
    @BindView(R.id.list_notification) RecyclerView listNotification;
    RecyclerView.LayoutManager layoutManager;

    private Notification notification;
    ApiCommand apiCommand;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notif);

        ButterKnife.bind(this);
        apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);
        notification = new Notification();
        layoutManager = new LinearLayoutManager(this);
        listNotification.setLayoutManager(layoutManager);


        obtainNotification();
        getSupportActionBar().setTitle("Notifikasi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void obtainNotification() {
        apiCommand.getNotification(App.sharedPrefManager.getUserDetails().get(SharedPrefManager.SESSION_ID)).enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                Log.e("_logResponse", response.toString() );
                notification= response.body();
                List<Notification.Data> data = notification.getData();
                Log.e("_logSize",String.valueOf(data.size()));

                listNotification.setAdapter(new NotificationAdapter(NotifActivity.this,data));


            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {

            }
        });
    }
}

package rachmanforniandi.com.yorozuyadistro.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.WayBillAdapter;
import rachmanforniandi.com.yorozuyadistro.sourceData.RajaOngkirKey;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.waybills.WayBill;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WayBillActivity extends AppCompatActivity {
    @BindView(R.id.list_waybill) RecyclerView listWayBill;
    @BindView(R.id.load_waybill) ProgressBar loadWayBill;
    @BindView(R.id.txt_receiver) TextView txtReceiver;
    @BindView(R.id.txt_status_order) TextView txtStatusOrder;

    Bundle bundle;
    private ApiCommand apiCommand;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_way_bill);
        ButterKnife.bind(this);

        apiCommand = APIProcessing.getLinkUrlRajaOngkir(RajaOngkirKey.PRO_BASE_URL_RAJA_ONGKIR).create(ApiCommand.class);
        bundle = getIntent().getExtras();

        layoutManager = new LinearLayoutManager(getApplicationContext());
        listWayBill.setLayoutManager(layoutManager);

        getSupportActionBar().setTitle("Riwayat Pengiriman");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadWayBillData(bundle.getString("WAYBILL"));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void loadWayBillData(String no_bill){
        apiCommand.getWayBillOfTransaction(RajaOngkirKey.KEY_PRO_RAJA_ONGKIR ,no_bill,"jne").enqueue(new Callback<WayBill>() {
            @Override
            public void onResponse(Call<WayBill> call, Response<WayBill> response){
                Log.e("_logResponseWaybill", response.toString() );

                WayBill.RajaOngkir.Result result = response.body().getRajaOngkir().getResult();

                WayBill.RajaOngkir.Result.DeliveryStatus status = result.getDeliveryStatus();
                txtReceiver.setText(status.getPod_receiver());
                txtStatusOrder.setText(status.getStatus());

                List<WayBill.RajaOngkir.Result.Manifest> manifests = result.getManifest();
                listWayBill.setAdapter(new WayBillAdapter(WayBillActivity.this,manifests));

                loadWayBill.setVisibility(View.GONE);
            }



            @Override
            public void onFailure(Call<WayBill> call, Throwable t) {

            }
        });
    }
}

package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.TransDetailAdapter;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransDetailActivity extends AppCompatActivity {

    @BindView(R.id.switch_bill) Switch switchBill;
    @BindView(R.id.switch_delivery) Switch switchDelivery;
    @BindView(R.id.linear_bill) LinearLayout linearBill;
    @BindView(R.id.linear_delivery)LinearLayout linearDelivery;
    @BindView(R.id.btn_track_transaction) Button btnTrackTransaction;
    @BindView(R.id.progressBar_detail_trans) ProgressBar progressBarDetailTrans;

    @BindView(R.id.tv_name_customer)TextView tvNameCustomer;
    @BindView(R.id.tv_status)TextView tvStatus;
    @BindView(R.id.tv_address_detail)TextView tvAddressDetail;
    @BindView(R.id.tv_code_transaction) TextView tvCodeTransaction;
    @BindView(R.id.tv_price_detail_trans)TextView tvPriceDetailTrans;

    @BindView(R.id.list_customer) RecyclerView listCustomer;
    Bundle bundle;
    ApiCommand apiCommand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_detail);
        ButterKnife.bind(this);

        apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);
        bundle = getIntent().getExtras();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listCustomer.setLayoutManager(layoutManager);

        switchBill.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    linearBill.setVisibility(View.VISIBLE);
                }else {
                    linearBill.setVisibility(View.GONE);
                }
            }
        });

        switchDelivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    linearDelivery.setVisibility(View.VISIBLE);
                }else {
                    linearDelivery.setVisibility(View.GONE);
                }
            }
        });
        tvCodeTransaction.setText(bundle.getString("TRANSACTION_CODE"));
        obtainedDataTransaction();

        getSupportActionBar().setTitle("Detail Transaksi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void obtainedDataTransaction(){
        apiCommand.getDataTransactionDetail(bundle.getString("TRANSACTION_CODE"))
                .enqueue(new Callback<TransDetail>() {
            @Override
            public void onResponse(Call<TransDetail> call, Response<TransDetail> response) {
                Log.e("_logResponseDetailTrans", response.toString() );
                final TransDetail.Data data = response.body().getData();

                tvPriceDetailTrans.setText(data.getGrandtotal());
                tvNameCustomer.setText(data.getUser());
                tvAddressDetail.setText(data.getDestination());
                tvStatus.setText(data.getStatus_transaction());

                if (data.getStatus_transaction().equals("sent")){
                    btnTrackTransaction.setVisibility(View.VISIBLE);
                    btnTrackTransaction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(TransDetailActivity.this,WayBillActivity.class);
                            intent.putExtra("WAYBILL",data.getResi_code());
                            startActivity(intent);
                        }
                    });
                }else {
                    btnTrackTransaction.setVisibility(View.GONE);
                }

                List<TransDetail.Data.DetailTransaction> transactions = data.getDetail_transaction();
                TransDetailAdapter transDetailAdapter = new TransDetailAdapter(TransDetailActivity.this,transactions);
                listCustomer.setAdapter(transDetailAdapter);

                progressBarDetailTrans.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<TransDetail> call, Throwable t) {

            }
        });
    }
}

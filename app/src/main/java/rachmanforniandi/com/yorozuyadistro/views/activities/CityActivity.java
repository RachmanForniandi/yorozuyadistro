package rachmanforniandi.com.yorozuyadistro.views.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.CityAdapter;
import rachmanforniandi.com.yorozuyadistro.sourceData.RajaOngkirKey;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.cities.City;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CityActivity extends AppCompatActivity {
    @BindView(R.id.list_of_city)
    RecyclerView listOfCity;

    @BindView(R.id.progressbar_city)
    ProgressBar progressBarCity;

    @BindView(R.id.et_city_name)
    EditText etCityName;

    CityAdapter cityAdapter;

    @OnClick(R.id.img_cancel)void clickCancel(){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        ButterKnife.bind(this);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listOfCity.setLayoutManager(layoutManager);

        getNameOfCities();
    }


    private void getNameOfCities(){
        ApiCommand apiCommand = APIProcessing.getLinkUrlRajaOngkir(RajaOngkirKey.STARTER_BASE_URL_RAJA_ONGKIR).create(ApiCommand.class);
        Call<City> call = apiCommand.getCities(RajaOngkirKey.KEY_RAJA_ONGKIR);
        call.enqueue(new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                Log.e("_logResponse", response.toString() );
                City.RajaOngkir rajaOngkir = response.body().getRajaOngkir();

                List<City.RajaOngkir.Results> results = rajaOngkir.getResults();

                cityAdapter = new CityAdapter(CityActivity.this,results);
                listOfCity.setAdapter(cityAdapter);

                progressBarCity.setVisibility(View.GONE);
                etCityName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String search = s.toString();

                        cityAdapter.getFilter().filter(search);
                    }
                });
            }

            @Override
            public void onFailure(Call<City> call, Throwable t) {

            }
        });
    }
}

package rachmanforniandi.com.yorozuyadistro.views.fragments;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xwray.passwordview.PasswordView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.users.User;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.AuthState;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.TheConverter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    @BindView(R.id.input_email)
    EditText inputEmail;

    @BindView(R.id.input_password)
    PasswordView inputPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this,view);


        return view;


    }


    @OnClick(R.id.btn_login)
    public void onClick(View view){
        if (inputEmail.length() >0 && inputPassword.length() >0){
            if (TheConverter.isValidEmailId(inputEmail.getText().toString())){
                Authentication(inputEmail.getText().toString(),inputPassword.getText().toString());


            }else {
                Toast.makeText(getContext(),"Mohon masukan input email anda dengan benar",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(getContext(),"Mohon masukan input data anda dengan benar",Toast.LENGTH_SHORT).show();
        }
    }

    private void Authentication(String email, final String pass){
        //Toast.makeText(getContext(),"Berhasil",Toast.LENGTH_SHORT).show();
        ApiCommand apiCommand = APIProcessing.getLinkUrl().create((ApiCommand.class));
        Call<User>call = apiCommand.authUser(email, pass);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){

                    User.Data userData = response.body().getData();
                    Toast.makeText(getContext(),userData.getName(),Toast.LENGTH_SHORT).show();

                    App.sharedPrefManager.MakeLoginSession(String.valueOf(userData.getId()), userData.getName(),
                            userData.getEmail(), pass);

                    App.sessionPref = App.sharedPrefManager.getUserDetails();
                    AuthState.updateToken(getActivity(),App.sessionPref.get(SharedPrefManager.SESSION_TOKEN));

                    getActivity().finish();

                }else {
                    Toast.makeText(getContext(),response.message(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }
}

package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.CartAdapter;
import rachmanforniandi.com.yorozuyadistro.sourceData.RajaOngkirKey;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.cart.Cart;

public class ItemCartActivity extends AppCompatActivity {

    public static TextView tvPriceTotal;
    @BindView(R.id.btn_to_checkout)
    Button btnToCheckOut;
    @BindView(R.id.list_item_cart)
    RecyclerView listItemCart;

    public static List<Cart> cartList = new ArrayList<>();
    public static CartAdapter cartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_cart);
        ButterKnife.bind(this);
        tvPriceTotal = findViewById(R.id.tv_price_total);

        cartList.clear();
        cartList.addAll(App.sqLiteDBHelper.userCart());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listItemCart.setLayoutManager(layoutManager);

        cartAdapter = new CartAdapter(this,cartList);
        listItemCart.setAdapter(cartAdapter);

        //App.sqLiteDBHelper.userCart();

        getSupportActionBar().setTitle("Keranjang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_to_checkout)
    public void onClick(View view){
        startActivity(new Intent(ItemCartActivity.this,DeliveryFeeActivity.class));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (RajaOngkirKey.PURCHASE_NOW){
            startActivity(new Intent(this,DeliveryFeeActivity.class));
            RajaOngkirKey.PURCHASE_NOW = false;
        }
    }
}

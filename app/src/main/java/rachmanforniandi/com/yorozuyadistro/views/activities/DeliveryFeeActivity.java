package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.RajaOngkirKey;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.costs.Cost;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransPost;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.TheConverter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryFeeActivity extends AppCompatActivity {

    @BindView(R.id.linearSave)
    LinearLayout linearSave;

    @BindView(R.id.linearTrans)
    LinearLayout linearTrans;

    @BindView(R.id.btnSave)
    Button btnSave;

    @BindView(R.id.edtDestination)
    EditText etDestination;

    @BindView(R.id.edtAddress)
    EditText etAddress;

    @BindView(R.id.spinnerServiceOption)
    Spinner spinnerServiceOption;

    @BindView(R.id.txtOngkir)
    TextView txtOngkir;

    @BindView(R.id.progressBarDelivery)
    ProgressBar progressBarDelivery;

    List<String> listServices;
    List<Integer> listValue;
    int deliveryFee;


    @OnClick(R.id.btnSave)void clickSave(){
        if (etDestination.length() > 0 || etAddress.length() > 0){
            progressBarDelivery.setVisibility(View.VISIBLE);
            linearSave.setVisibility(View.GONE);

            ArrayList<TransPost.DetailItem> arrayList = new ArrayList<>();
            for (int i=0; i< ItemCartActivity.cartList.size();i++){
                TransPost.DetailItem detailItem = new TransPost().new DetailItem();
                detailItem.setProductId(ItemCartActivity.cartList.get(i).getId_product());
                detailItem.setQty(ItemCartActivity.cartList.get(i).getQuantity());
                detailItem.setPrice(ItemCartActivity.cartList.get(i).getPrice());
                detailItem.setTotal(ItemCartActivity.cartList.get(i).getTotal());

                arrayList.add(detailItem);
            }
            
            TransPost transPost = new TransPost();
            transPost.setUserId(Integer.parseInt(App.sharedPrefManager.getUserDetails().get(SharedPrefManager.SESSION_ID)));
            transPost.setDestination(etDestination.getText().toString() + " - " + etAddress.getText().toString());
            transPost.setOngkir(deliveryFee);
            transPost.setGrandtotal(ItemCartActivity.cartAdapter.getPriceTotal()+deliveryFee);
            transPost.setDetail(arrayList);

            checkoutTransaction(transPost);
        }else {
            Toast.makeText(this, "Mohon lengkapi alamat pengiriman untuk memudahkan pengiriman barang anda", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.edtDestination)void clickedForDestination(){
        startActivity(new Intent(this,CityActivity.class));
    }

    @OnClick(R.id.btnTrans)void clickToTransfer(){
        startActivity(new Intent(this, TransactionActivity.class));
        finish();
    }
    @OnClick(R.id.txtDismiss)void clickToDismiss(){
        finish();
    }

    @OnClick(R.id.txtCancel)void clickToCancel(){
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_fee);
        ButterKnife.bind(this);

        listServices = new ArrayList<>();
        listValue = new ArrayList<>();

        getSupportActionBar().setTitle("Cek Biaya Pengiriman");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!RajaOngkirKey.DESTINATION.equals("")){
            etDestination.setText(RajaOngkirKey.DESTINATION_NAME);

            getCostService();
        }
    }

    private void getCostService(){
        listValue.clear();
        listServices.clear();

        ApiCommand apiCommand = APIProcessing.getLinkUrlRajaOngkir(RajaOngkirKey.STARTER_BASE_URL_RAJA_ONGKIR).create(ApiCommand.class);
        Call<Cost> call = apiCommand.getCostValue(RajaOngkirKey.KEY_RAJA_ONGKIR,"444",RajaOngkirKey.DESTINATION,"1000","jne");
        call.enqueue(new Callback<Cost>() {
            @Override
            public void onResponse(Call<Cost> call, Response<Cost> response) {
                Log.e("_logResponse", response.toString() );
                Cost.Rajaongkir feeDeliver = response.body().getRajaongkir();

                List<Cost.Rajaongkir.Results> results = feeDeliver.getResults();
                for (int i = 0; i < results.size(); i++){
                    Log.e("_logServiceCode", results.get(i).getCode());

                    List<Cost.Rajaongkir.Results.Costs> costs = results.get(i).getCosts();
                    for (int j = 0; j < costs.size(); j++){
                        Log.e("_logServiceDesc", costs.get(j).getDescription());

                        List<Cost.Rajaongkir.Results.Costs.Data> data = costs.get(j).getCost();
                        for (int k = 0; k < data.size(); k++){
                            Log.e("_logServiceValue", String.valueOf(data.get(k).getValue()));

                            listServices.add("Rp " + TheConverter.rupiah(data.get(k).getValue()) +
                                    " (JNE " + costs.get(j).getService()+ ")");

                            listValue.add(data.get(k).getValue());
                        }
                    }
                }

                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(DeliveryFeeActivity.this,
                        android.R.layout.simple_list_item_1,listServices);
                spinnerServiceOption.setAdapter(arrayAdapter);

                spinnerServiceOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        txtOngkir.setText("Rp " + TheConverter.rupiah(listValue.get(position)));
                        deliveryFee = listValue.get(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<Cost> call, Throwable t) {

            }
        });
    }

    private void checkoutTransaction(TransPost transPost){
        ApiCommand apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);
        Call<TransPost> call = apiCommand.insertDataTransaction(transPost);
        call.enqueue(new Callback<TransPost>() {
            @Override
            public void onResponse(Call<TransPost> call, Response<TransPost> response) {
                if (response.isSuccessful()){
                    linearTrans.setVisibility(View.VISIBLE);
                    progressBarDelivery.setVisibility(View.GONE);

                    Toast.makeText(DeliveryFeeActivity.this, "Transaksi berhasil dibuat", Toast.LENGTH_LONG).show();
                    App.sqLiteDBHelper.clearTable();
                }
            }

            @Override
            public void onFailure(Call<TransPost> call, Throwable t) {

            }
        });
    }
}

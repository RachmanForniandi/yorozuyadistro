package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.uploads.Upload;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.FileUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadReceiptActivity extends AppCompatActivity {
    @BindView(R.id.img_display_pict)
    ImageView imgDisplayView;
    @BindView(R.id.progressbar_upload)
    ProgressBar progressbarUpload;
    @BindView(R.id.btn_upload)
    Button btnUpload;

    @BindView(R.id.btn_select_from_gallery)
    Button btnSelectGallery;
    @BindView(R.id.border_display)
    LinearLayout borderDisplay;

    private static final int ACTION_PICK_IMAGE_FROM_GALLERY=100;
    private Uri uri;
    Bundle bundle;
    ApiCommand apiCommand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_receipt);
        ButterKnife.bind(this);
        apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);

        bundle = getIntent().getExtras();
        Log.e("codeTrans",bundle.getString("TRANSACTION_CODE"));

        getSupportActionBar().setTitle("Upload bukti transfer pembayaran");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick({R.id.btn_select_from_gallery,R.id.btn_upload})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_select_from_gallery:
                permissionGallery();
                break;

            case R.id.btn_upload:
                if (uri != null){
                    File file = FileUtils.getFile(getApplicationContext(),uri);
                    uploadImage(file);

                    borderDisplay.setVisibility(View.GONE);
                    progressbarUpload.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    private void accessGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"select Image"),ACTION_PICK_IMAGE_FROM_GALLERY);
    }

    private void permissionGallery() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

        } else {
            accessGallery();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTION_PICK_IMAGE_FROM_GALLERY && resultCode == Activity.RESULT_OK){
            uri = data.getData();
            imgDisplayView.setImageURI(uri);

            btnSelectGallery.setText("Ganti gambar");
            btnUpload.setVisibility(View.VISIBLE);
        }

    }

    private void uploadImage(File fileImg){
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"),fileImg);
        MultipartBody.Part img = MultipartBody.Part.createFormData("foto",fileImg.getName(),requestFile);

        apiCommand.uploadImg(bundle.getString("TRANSACTION_CODE"),img).enqueue(new Callback<Upload>() {
            @Override
            public void onResponse(Call<Upload> call, Response<Upload> response) {
                Log.e("_logResponseUpload", response.toString() );
                if (response.code() == 202){
                    Toast.makeText(getApplicationContext(), "Bukti pembayaran berhasil diupload.", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(), "Upload bukti pembayaran gagal", Toast.LENGTH_LONG).show();
                }
                borderDisplay.setVisibility(View.VISIBLE);
                progressbarUpload.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<Upload> call, Throwable t) {

            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}

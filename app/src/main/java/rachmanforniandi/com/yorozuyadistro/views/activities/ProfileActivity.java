package rachmanforniandi.com.yorozuyadistro.views.activities;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xwray.passwordview.PasswordView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.users.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    @BindView(R.id.linear_profile_data) LinearLayout linearProfileData;
    @BindView(R.id.linear_profile_edit) LinearLayout linearProfileEdit;
    @BindView(R.id.tv_name_profile)TextView tvNameProfile;
    @BindView(R.id.tv_email_profile)TextView tvEmailProfile;
    @BindView(R.id.tv_password_profile)TextView tvPasswordProfile;
    @BindView(R.id.et_name) EditText etNameProfile;
    @BindView(R.id.et_email) EditText etEmailProfile;
    @BindView(R.id.et_current_password)PasswordView etCurrentPassword;
    @BindView(R.id.et_new_password) PasswordView etNewPassword;
    @BindView(R.id.et_confirm_new_password)PasswordView etConfirmNewPassword;
    @BindView(R.id.btn_confirm_new_password)Button btnConfirmNewPassword;
    @BindView(R.id.toolbar)Toolbar toolbar;
    @BindView(R.id.fab)FloatingActionButton fab;

    boolean editProfile = false;
    HashMap<String, String> sessionPreferences;
    String user_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        sessionPreferences = App.sharedPrefManager.getUserDetails();
        user_id = sessionPreferences.get(App.sharedPrefManager.SESSION_ID);
        tvNameProfile.setText(sessionPreferences.get(App.sharedPrefManager.SESSION_NAME));
        tvEmailProfile.setText(sessionPreferences.get(App.sharedPrefManager.SESSION_EMAIL));
        tvPasswordProfile.setText(sessionPreferences.get(App.sharedPrefManager.SESSION_PASS));

        etNameProfile.setText(sessionPreferences.get(App.sharedPrefManager.SESSION_NAME));
        etEmailProfile.setText(sessionPreferences.get(App.sharedPrefManager.SESSION_EMAIL));

    }



    @OnClick({R.id.fab,R.id.btn_confirm_new_password})
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fab:
                if (editProfile){
                    editTrue();
                }else {
                    editFalse();
                }
                break;

            case R.id.btn_confirm_new_password:
                if (!etCurrentPassword.getText().toString().equals(sessionPreferences.get(App.sharedPrefManager.SESSION_PASS))){
                    Toast.makeText(getApplicationContext(), "Password anda salah.", Toast.LENGTH_SHORT).show();
                }else if (!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString())){
                    Toast.makeText(getApplicationContext(), "Mohon Konfirmasi password baru dengan benar.", Toast.LENGTH_SHORT).show();
                }else if (etNewPassword.length()<=0){
                    Toast.makeText(getApplicationContext(), "Kolom password baru tidak boleh kosong.", Toast.LENGTH_SHORT).show();
                }else {
                    setUpdate();
                }
                break;
        }

    }

    private void setUpdate() {
        //Toast.makeText(this, "Berhasil", Toast.LENGTH_SHORT).show();
        ApiCommand apiCommand = APIProcessing.getLinkUrl().create((ApiCommand.class));
        Call<User> call = apiCommand.changeProfileUser(user_id,etNameProfile.getText().toString(),
                etEmailProfile.getText().toString(),etNewPassword.getText().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){

                    App.sharedPrefManager.userLogout();
                    App.sharedPrefManager.MakeLoginSession(
                            user_id,etNameProfile.getText().toString(),
                            etEmailProfile.getText().toString(),
                            etNewPassword.getText().toString()
                    );

                    editTrue();
                }else {
                    Toast.makeText(getApplicationContext(),response.message(),Toast.LENGTH_SHORT)
                            .show();
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    private void editTrue(){
        linearProfileData.setVisibility(View.VISIBLE);
        linearProfileEdit.setVisibility(View.GONE);
        fab.setImageDrawable(ContextCompat.getDrawable(ProfileActivity.this,R.drawable.ic_fab_edit));
        editProfile = false;
    }

    private void editFalse(){
        linearProfileData.setVisibility(View.GONE);
        linearProfileEdit.setVisibility(View.VISIBLE);
        fab.setImageDrawable(ContextCompat.getDrawable(ProfileActivity.this,R.drawable.ic_fab_close));
        editProfile = true;
    }


}

package rachmanforniandi.com.yorozuyadistro.views.fragments.transaction;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.TransPaidAdapter;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransUser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaidFragment extends Fragment {

    @BindView(R.id.list_paid)
    RecyclerView listPaid;

    @BindView(R.id.txt_no_data_trans)
    TextView txtNoDataTrans;

    private TransUser transUser;
    private RecyclerView.LayoutManager layoutManager;
    ApiCommand apiCommand;


    public PaidFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_paid, container, false);
        ButterKnife.bind(this,view);

        apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);
        transUser = new TransUser();
        layoutManager = new LinearLayoutManager(getActivity());
        listPaid.setLayoutManager(layoutManager);

        showDataPaidTransaction();

        return view;
    }

    private void showDataPaidTransaction() {
        apiCommand.getDataTransactionPaid(App.sharedPrefManager.getUserDetails().get(SharedPrefManager.SESSION_ID)).enqueue(new Callback<TransUser>() {
            @Override
            public void onResponse(Call<TransUser> call, Response<TransUser> response) {
                Log.e("_logResponsePaid", response.toString() );
                if (response.isSuccessful()){
                    transUser = response.body();
                    List<TransUser.Data> data = transUser.getData();

                    Log.e("_logSizePaidData",String.valueOf(data.size()));
                    listPaid.setAdapter(new TransPaidAdapter(getActivity(), data));
                }else {
                    txtNoDataTrans.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onFailure(Call<TransUser> call, Throwable t) {

            }
        });
    }

}

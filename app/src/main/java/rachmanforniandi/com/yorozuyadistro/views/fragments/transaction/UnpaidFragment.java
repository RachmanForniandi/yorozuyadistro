package rachmanforniandi.com.yorozuyadistro.views.fragments.transaction;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.adapters.TransUnpaidAdapter;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransUser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UnpaidFragment extends Fragment {
    @BindView(R.id.list_unpaid)
    RecyclerView listUnpaid;

    @BindView(R.id.txt_no_data_trans)
    TextView txtNoDataTrans;

    private TransUser transUser;
    private RecyclerView.LayoutManager layoutManager;
    ApiCommand apiCommand;


    public UnpaidFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_unpaid, container, false);
        ButterKnife.bind(this,view);

        apiCommand = APIProcessing.getLinkUrl().create(ApiCommand.class);
        transUser = new TransUser();
        layoutManager = new LinearLayoutManager(getContext());
        listUnpaid.setLayoutManager(layoutManager);

        showDataTransaction();

        return view;
    }

    private void showDataTransaction() {
        Call<TransUser> call = apiCommand.getDataTransactionUnpaid(App.sharedPrefManager.getUserDetails().get(SharedPrefManager.SESSION_ID));

        call.enqueue(new Callback<TransUser>() {
            @Override
            public void onResponse(Call<TransUser> call, Response<TransUser> response) {
                Log.e("_logResponseUnpaid", response.toString() );
                if (response.isSuccessful()){
                    transUser = response.body();
                    List<TransUser.Data> data = transUser.getData();

                    Log.e("_logSizeUnpaid",String.valueOf(data.size()));
                    listUnpaid.setAdapter(new TransUnpaidAdapter(getActivity(), data));
                }else {
                    txtNoDataTrans.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onFailure(Call<TransUser> call, Throwable t) {

            }
        });
    }

}

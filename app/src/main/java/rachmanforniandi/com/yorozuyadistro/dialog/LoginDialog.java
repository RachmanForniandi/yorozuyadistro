package rachmanforniandi.com.yorozuyadistro.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.xwray.passwordview.PasswordView;

import rachmanforniandi.com.yorozuyadistro.App;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.APIProcessing;
import rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities.ApiCommand;
import rachmanforniandi.com.yorozuyadistro.sourceData.database.SharedPrefManager;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.users.User;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.AuthState;
import rachmanforniandi.com.yorozuyadistro.supportUtilities.TheConverter;
import rachmanforniandi.com.yorozuyadistro.views.activities.RegisterActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginDialog {
    /*@BindView(R.id.btn_login)
    Button btnToLogin;

    @BindView(R.id.txt_link_register)
    TextView txtLinkToRegister;*/

    public void showLoginDialog(final Context context, Menu menu){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_login,null);
        builder.setView(view);

        final EditText etEmail = view.findViewById(R.id.input_email);
        final PasswordView etPass = view.findViewById(R.id.input_password);

        final AlertDialog alertDialog = builder.create();

        view.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etEmail.length() > 0 && etPass.length() >0){
                    String email = etEmail.getText().toString().trim();
                    final String pass = etPass.getText().toString().trim();

                    if (TheConverter.isValidEmailId(email)){

                        ApiCommand apiCommand = APIProcessing.getLinkUrl().create((ApiCommand.class));
                        Call<User> call = apiCommand.authUser(email, pass);
                        call.enqueue(new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                if (response.isSuccessful()){

                                    User.Data userData = response.body().getData();

                                    AuthState.isLoggedIn(menu);
                                    App.sharedPrefManager.MakeLoginSession(String.valueOf(userData.getId()), userData.getName(),userData.getEmail(), pass);

                                    App.sessionPref = App.sharedPrefManager.getUserDetails();
                                    AuthState.updateToken(context,App.sessionPref.get(SharedPrefManager.SESSION_TOKEN));

                                    alertDialog.dismiss();
                                }else {
                                    Toast.makeText(context,response.message(),Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {

                            }
                        });
                    }
                }
            }
        });
        view.findViewById(R.id.txt_link_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, RegisterActivity.class));
                alertDialog.dismiss();
            }
        });

        alertDialog.show();

    }

}

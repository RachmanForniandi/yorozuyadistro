package rachmanforniandi.com.yorozuyadistro.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import butterknife.BindView;
import butterknife.OnClick;
import rachmanforniandi.com.yorozuyadistro.R;
import rachmanforniandi.com.yorozuyadistro.sourceData.RajaOngkirKey;
import rachmanforniandi.com.yorozuyadistro.views.activities.ItemCartActivity;

public class CartDialog {

    /*@BindView(R.id.btn_to_cart)
    Button btnToCart;*/

    public void showCartDialog(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_cart,null);
        builder.setView(view);

        view.findViewById(R.id.btn_to_cart).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ItemCartActivity.class));
                ((Activity)context).finish();
            }
        });

        view.findViewById(R.id.btn_to_payment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RajaOngkirKey.PURCHASE_NOW = true;
                context.startActivity(new Intent(context, ItemCartActivity.class));
                ((Activity)context).finish();
            }
        });


        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        /*alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.dismiss();*/
    }
    /*@OnClick(R.id.btn_to_cart)
    public void onClick(View view){

    }*/

}

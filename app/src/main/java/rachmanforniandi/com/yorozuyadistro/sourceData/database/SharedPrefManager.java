package rachmanforniandi.com.yorozuyadistro.sourceData.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.HashMap;

public class SharedPrefManager {

    private SharedPreferences prefManager;
    private SharedPreferences.Editor editor;

    Context context;

    private int PRIVATE_MODE = 0;

    public static final String PREF_NAME = "YorozuyaDistro";
    public static final String IS_LOGIN = "isLoggedIn";
    public static final String SESSION_ID = "id";
    public static final String SESSION_NAME = "name";
    public static final String SESSION_EMAIL = "email";
    public static final String SESSION_PASS = "password";
    public static final String SESSION_TOKEN = "token";

    public SharedPrefManager(Context context) {
        this.context = context;
        prefManager = context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = prefManager.edit();
    }

    public void MakeLoginSession(String id,String name,String email,String password){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(SESSION_ID, id);
        editor.putString(SESSION_NAME, name);
        editor.putString(SESSION_EMAIL, email);
        editor.putString(SESSION_PASS, password);

        editor.commit();
        Toast.makeText(context,"Selamat datang "+ name,Toast.LENGTH_SHORT).show();

    }

    public void MakeTokenSession(String token){

        editor.putString(SESSION_TOKEN, token);

        editor.commit();
        //Toast.makeText(context,"Selamat datang "+ token,Toast.LENGTH_SHORT).show();

    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<>();

        user.put(SESSION_ID, prefManager.getString(SESSION_ID, null));
        user.put(SESSION_NAME, prefManager.getString(SESSION_NAME, null));
        user.put(SESSION_EMAIL, prefManager.getString(SESSION_EMAIL, null));
        user.put(SESSION_PASS, prefManager.getString(SESSION_PASS, null));
        user.put(SESSION_TOKEN, prefManager.getString(SESSION_TOKEN, null));

        return user;
    }

    public void userLogout(){
        //editor.clear();
        MakeLoginSession(null,null,null,null);
        editor.putBoolean(IS_LOGIN,false);
        editor.commit();

        Toast.makeText(context, "Keluar", Toast.LENGTH_SHORT).show();
    }

    public boolean isLoggedIn(){
        return prefManager.getBoolean(IS_LOGIN, false);
    }

}

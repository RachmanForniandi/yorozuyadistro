package rachmanforniandi.com.yorozuyadistro.sourceData.models.notifications;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class Notification{

	@SerializedName("data")
	private List<Data> data;

	public void setData(List<Data> data){
		this.data = data;
	}

	public List<Data> getData(){
		return data;
	}


	public class Data {

		@SerializedName("transaction_id")
		private String transactionId;

		@SerializedName("transaction_code")
		private String transactionCode;

		@SerializedName("updated_at")
		private String updatedAt;

		@SerializedName("user_id")
		private String userId;

		@SerializedName("description")
		private String description;

		@SerializedName("created_at")
		private String createdAt;

		@SerializedName("id")
		private int id;

		public void setTransactionId(String transactionId){
			this.transactionId = transactionId;
		}

		public String getTransactionId(){
			return transactionId;
		}

		public void setTransactionCode(String transactionCode){
			this.transactionCode = transactionCode;
		}

		public String getTransactionCode(){
			return transactionCode;
		}

		public void setUpdatedAt(String updatedAt){
			this.updatedAt = updatedAt;
		}

		public String getUpdatedAt(){
			return updatedAt;
		}

		public void setUserId(String userId){
			this.userId = userId;
		}

		public String getUserId(){
			return userId;
		}

		public void setDescription(String description){
			this.description = description;
		}

		public String getDescription(){
			return description;
		}

		public void setCreatedAt(String createdAt){
			this.createdAt = createdAt;
		}

		public String getCreatedAt(){
			return createdAt;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}
	}
}
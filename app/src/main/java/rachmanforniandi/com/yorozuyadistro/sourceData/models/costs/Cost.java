package rachmanforniandi.com.yorozuyadistro.sourceData.models.costs;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cost{

	@SerializedName("rajaongkir")
	private Rajaongkir rajaongkir;

	public void setRajaongkir(Rajaongkir rajaongkir){
		this.rajaongkir = rajaongkir;
	}

	public Rajaongkir getRajaongkir(){
		return rajaongkir;
	}


	public class Rajaongkir{

		@SerializedName("query")
		private Query query;

		@SerializedName("destination_details")
		private DestinationDetails destinationDetails;

		@SerializedName("origin_details")
		private OriginDetails originDetails;

		@SerializedName("results")
		private List<Results> results;

		@SerializedName("status")
		private Status status;

		public void setQuery(Query query){
			this.query = query;
		}

		public Query getQuery(){
			return query;
		}

		public void setDestinationDetails(DestinationDetails destinationDetails){
			this.destinationDetails = destinationDetails;
		}

		public DestinationDetails getDestinationDetails(){
			return destinationDetails;
		}

		public void setOriginDetails(OriginDetails originDetails){
			this.originDetails = originDetails;
		}

		public OriginDetails getOriginDetails(){
			return originDetails;
		}

		public void setResults(List<Results> results){
			this.results = results;
		}

		public List<Results> getResults(){
			return results;
		}

		public void setStatus(Status status){
			this.status = status;
		}

		public Status getStatus(){
			return status;
		}

		public class Results {

			@SerializedName("costs")
			private List<Costs> costs;

			@SerializedName("code")
			private String code;

			@SerializedName("name")
			private String name;

			public void setCosts(List<Costs> costs){
				this.costs = costs;
			}

			public List<Costs> getCosts(){
				return costs;
			}

			public void setCode(String code){
				this.code = code;
			}

			public String getCode(){
				return code;
			}

			public void setName(String name){
				this.name = name;
			}

			public String getName(){
				return name;
			}

			public class Costs {

				@SerializedName("cost")
				private List<Data> cost;

				@SerializedName("service")
				private String service;

				@SerializedName("description")
				private String description;

				public void setCost(List<Data> cost){
					this.cost = cost;
				}

				public List<Data> getCost(){
					return cost;
				}

				public void setService(String service){
					this.service = service;
				}

				public String getService(){
					return service;
				}

				public void setDescription(String description){
					this.description = description;
				}

				public String getDescription(){
					return description;
				}

				public class Data {

					@SerializedName("note")
					private String note;

					@SerializedName("etd")
					private String etd;

					@SerializedName("value")
					private int value;

					public void setNote(String note){
						this.note = note;
					}

					public String getNote(){
						return note;
					}

					public void setEtd(String etd){
						this.etd = etd;
					}

					public String getEtd(){
						return etd;
					}

					public void setValue(int value){
						this.value = value;
					}

					public int getValue(){
						return value;
					}
				}
			}
		}
	}
}
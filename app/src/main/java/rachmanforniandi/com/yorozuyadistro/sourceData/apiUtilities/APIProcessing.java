package rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIProcessing {

    private static Retrofit retrofit;
    public static Retrofit getLinkUrl(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.neoyorozuya.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    public static Retrofit getLinkUrlRajaOngkir(String link_url){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(link_url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }


}

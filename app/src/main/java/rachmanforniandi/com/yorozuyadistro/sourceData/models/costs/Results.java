package rachmanforniandi.com.yorozuyadistro.sourceData.models.costs;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Results {

	@SerializedName("costs")
	private List<Costs> costs;

	@SerializedName("code")
	private String code;

	@SerializedName("name")
	private String name;

	public void setCosts(List<Costs> costs){
		this.costs = costs;
	}

	public List<Costs> getCosts(){
		return costs;
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}
}
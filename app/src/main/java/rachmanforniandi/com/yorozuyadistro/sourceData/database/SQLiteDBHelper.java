package rachmanforniandi.com.yorozuyadistro.sourceData.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import rachmanforniandi.com.yorozuyadistro.sourceData.models.cart.Cart;

public class SQLiteDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME ="JackAllTradeDistro";
    private static final int DATABASE_VERSION = 1;

    private static SQLiteDatabase sqLiteDatabase;

    private static final String TABLE_NAME = "cart";
    private static final String ID_CART = "id_cart";
    private static final String ID_PRODUCT = "id_product";
    private static final String PRODUCT = "product";
    private static final String IMAGE_URL = "image";
    private static final String PRICE = "price";
    private static final String CURRENT_DATE = "curr_date";

    public SQLiteDBHelper(Context context) {
        super(context, DATABASE_NAME,null,DATABASE_VERSION);
        sqLiteDatabase = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" +
                ID_CART +" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                ID_PRODUCT +" INTEGER, "+
                PRODUCT + " TEXT, "+
                IMAGE_URL + " TEXT, " +
                PRICE + " INTEGER, "+
                CURRENT_DATE + " DATE DEFAULT CURRENT_DATE );"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public Long addToCart( int id_product,String product, String image, int price){
        ContentValues values = new ContentValues();
        values.put(ID_PRODUCT, id_product);
        values.put(PRODUCT, product);
        values.put(IMAGE_URL, image);
        values.put(PRICE, price);

        long id_cart = sqLiteDatabase.insert(TABLE_NAME,null,values);
        return id_cart;

    }

    public Integer checkExists(int id_product){
        String sql = "SELECT " + ID_PRODUCT+ " FROM " + TABLE_NAME + " WHERE " + ID_PRODUCT + "='" + String.valueOf(id_product) + "'";
        sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(sql,null);

        int count = cursor.getCount();
        return count;
    }

    public List<Cart> userCart(){
        String sqlQuery = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + ID_CART +" DESC";

        ArrayList<Cart> carts = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery(sqlQuery,null);
        cursor.moveToFirst();

        for (int i =0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);

            Cart cart = new Cart();
            cart.setId_cart(cursor.getInt(cursor.getColumnIndex(ID_CART)));
            cart.setId_product(cursor.getInt(cursor.getColumnIndex(ID_PRODUCT)));
            cart.setProduct(cursor.getString(cursor.getColumnIndex(PRODUCT)));
            cart.setImage(cursor.getString(cursor.getColumnIndex(IMAGE_URL)));
            cart.setPrice(cursor.getInt(cursor.getColumnIndex(PRICE)));
            cart.setCurr_date(cursor.getString(cursor.getColumnIndex(CURRENT_DATE)));

            Log.e("_logProductName", cursor.getString(cursor.getColumnIndex(PRODUCT)));
            Log.e("_logCurrentDate", cursor.getString(cursor.getColumnIndex(CURRENT_DATE)));
            carts.add(cart);
        }

        return carts;
    }

    public void removeItem(String id_cart){
        sqLiteDatabase.delete(TABLE_NAME,ID_CART + "='"+ id_cart + "'",null);
    }

    public void clearTable(){
        sqLiteDatabase.delete(TABLE_NAME, null,null);
    }

}

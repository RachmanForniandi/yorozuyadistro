package rachmanforniandi.com.yorozuyadistro.sourceData.models.users;


import com.google.gson.annotations.SerializedName;


public class User{

	@SerializedName("data")
	private Data data;

	@SerializedName("status")
	private Status status;



	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setStatus(Status status){
		this.status = status;
	}

	public Status getStatus(){
		return status;
	}

	public class Data{

		@SerializedName("is_admin")
		private int isAdmin;

		@SerializedName("api_token")
		private String apiToken;

		@SerializedName("name")
		private String name;

		@SerializedName("id")
		private int id;

		@SerializedName("email")
		private String email;

		@SerializedName("username")
		private String username;

		public void setIsAdmin(int isAdmin){
			this.isAdmin = isAdmin;
		}

		public int getIsAdmin(){
			return isAdmin;
		}

		public void setApiToken(String apiToken){
			this.apiToken = apiToken;
		}

		public String getApiToken(){
			return apiToken;
		}

		public void setName(String name){
			this.name = name;
		}

		public String getName(){
			return name;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setEmail(String email){
			this.email = email;
		}

		public String getEmail(){
			return email;
		}

		public void setUsername(String username){
			this.username = username;
		}

		public String getUsername(){
			return username;
		}
	}


}
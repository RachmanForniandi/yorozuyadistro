package rachmanforniandi.com.yorozuyadistro.sourceData.models.detail;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Detail{

	@SerializedName("data")
	private Data data;

	@SerializedName("status")
	private Status status;

	/*@SerializedName("product")
	private String product;*/

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setStatus(Status status){
		this.status = status;
	}

	public Status getStatus(){
		return status;
	}


	public class Data{

		@SerializedName("image")
		private String image;

		@SerializedName("product")
		private String product;

		@SerializedName("images")
		private List<Images> images;

		@SerializedName("price")
		private int price;

		@SerializedName("description")
		private String description;

		@SerializedName("id")
		private int id;

		@SerializedName("stock")
		private int stock;

		public void setImage(String image){
			this.image = image;
		}

		public String getImage(){
			return image;
		}

		public void setProduct(String product){
			this.product = product;
		}

		public String getProduct(){
			return product;
		}

		public void setImages(List<Images> images){
			this.images = images;
		}

		public List<Images> getImages(){
			return images;
		}

		public void setPrice(int price){
			this.price = price;
		}

		public int getPrice(){
			return price;
		}

		public void setDescription(String description){
			this.description = description;
		}

		public String getDescription(){
			return description;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setStock(int stock){
			this.stock = stock;
		}

		public int getStock(){
			return stock;
		}

		public class Images {
			@SerializedName("image")
			private String image;

			@SerializedName("id")
			private int id;

			public void setImage(String image){
				this.image = image;
			}

			public String getImage(){
				return image;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}
		}

	}


	public class Status{

		@SerializedName("code")
		private int code;

		@SerializedName("description")
		private String description;

		public void setCode(int code){
			this.code = code;
		}

		public int getCode(){
			return code;
		}

		public void setDescription(String description){
			this.description = description;
		}

		public String getDescription(){
			return description;
		}


	}
}
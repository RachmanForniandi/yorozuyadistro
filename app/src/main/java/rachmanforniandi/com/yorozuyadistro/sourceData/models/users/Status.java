package rachmanforniandi.com.yorozuyadistro.sourceData.models.users;

import com.google.gson.annotations.SerializedName;


public class Status{

	@SerializedName("message")
	private String message;

	@SerializedName("Code")
	private int code;

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}
}
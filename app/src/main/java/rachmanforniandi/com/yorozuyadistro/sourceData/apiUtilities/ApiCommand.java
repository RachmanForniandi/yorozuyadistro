package rachmanforniandi.com.yorozuyadistro.sourceData.apiUtilities;

import okhttp3.MultipartBody;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.cities.City;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.costs.Cost;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.detail.Detail;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.notifications.Notification;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.products.Product;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransDetail;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransPost;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions.TransUser;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.uploads.Upload;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.users.User;
import rachmanforniandi.com.yorozuyadistro.sourceData.models.waybills.WayBill;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiCommand {

    @GET("products")
    Call<Product> getProducts();

    @GET("product/{id}")
    Call<Detail> getDetail(@Path ("id") int id);

    @FormUrlEncoded
    @POST("auth/login")
    Call<User> authUser(@Field("email")String email,
                       @Field("password")String password);

    @FormUrlEncoded
    @POST("auth/register")
    Call<User> authRegister(
                        @Field("name")String name,
                        @Field("email")String email,
                        @Field("password")String password);

    @FormUrlEncoded
    @POST("auth/update/{id}")
    Call<User> changeProfileUser(
            @Path("id")String id,
            @Field("name")String name,
            @Field("email")String email,
            @Field("password")String password);


    @POST("transaction")
    Call<TransPost> insertDataTransaction(
            @Body TransPost transPost);

    @GET("transaction/{code}")
    Call<TransDetail> getDataTransactionDetail(@Path("code")String code );

    @GET("transaction-user/{id}/unpaid")
    Call<TransUser> getDataTransactionUnpaid(@Path ("id") String id);

    @GET("transaction-user/{id}/paid")
    Call<TransUser> getDataTransactionPaid(@Path ("id") String id);

    @GET("notification/{id}")
    Call<Notification> getNotification(@Path ("id") String user_id);

    @GET("transaction-user/{id}")
    Call<TransUser> getDataTransaction(
            @Path("id") String user_id);

    @Multipart
    @POST("upload/{code}")
    Call<Upload> uploadImg(
            @Path("code")String code,
            @Part MultipartBody.Part file
            );

    @GET("city")
    Call<City>getCities(@Query("key")String key);

    @FormUrlEncoded
    @POST("cost")
    Call<Cost>getCostValue(
            @Field("key")String key,
            @Field("origin")String origin,
            @Field("destination")String destination,
            @Field("weight") String weight,
            @Field("courier")String courier
    );

    @FormUrlEncoded
    @POST("waybill")
    Call<WayBill>getWayBillOfTransaction(
            @Field("key")String key,
            @Field("waybill")String waybill,
            @Field("courier")String courier
    );

    @FormUrlEncoded
    @POST("auth/firebase-token/{id}")
    Call<User>updateTokenFireBase(
            @Path("id")String user_id,
            @Field("token")String token
    );


}

package rachmanforniandi.com.yorozuyadistro.sourceData.models.products;

import com.google.gson.annotations.SerializedName;


public class ProductResponse{


	@SerializedName("meta")
	private Meta meta;

	@SerializedName("links")
	private Links links;

	@SerializedName("status")
	private Status status;


	public void setMeta(Meta meta){
		this.meta = meta;
	}

	public Meta getMeta(){
		return meta;
	}

	public void setLinks(Links links){
		this.links = links;
	}

	public Links getLinks(){
		return links;
	}

	public void setStatus(Status status){
		this.status = status;
	}

	public Status getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProductResponse{" +
			",meta = '" + meta + '\'' + 
			",links = '" + links + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
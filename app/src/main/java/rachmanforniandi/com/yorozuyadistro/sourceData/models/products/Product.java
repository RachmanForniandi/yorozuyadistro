package rachmanforniandi.com.yorozuyadistro.sourceData.models.products;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Product {

	@SerializedName("data")
	public List<Data> products;

	public void setProducts(List<Data> products){
		this.products = products;
	}

	public List<Data> getProducts(){
		return products;
	}

	public class Data {

		@SerializedName("image")
		private String image;

		@SerializedName("product")
		private String product;

		@SerializedName("price")
		private int price;

		/*@SerializedName("description")
		private String description;*/

		@SerializedName("id")
		private int id;

		@SerializedName("stock")
		private int stock;

		public void setImage(String image){
			this.image = image;
		}

		public String getImage(){
			return image;
		}

		public void setProduct(String product){
			this.product = product;
		}

		public String getProduct(){
			return product;
		}

		public void setPrice(int price){
			this.price = price;
		}

		public int getPrice(){
			return price;
		}

		/*public void setDescription(String description){
			this.description = description;
		}

		public String getDescription(){
			return description;
		}*/

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setStock(int stock){
			this.stock = stock;
		}

		public int getStock(){
			return stock;
		}

		@Override
		public String toString(){
			return
					"Product{" +
                            "products = '" + products + '\'' +
							"image = '" + image + '\'' +
							",product = '" + product + '\'' +
							",price = '" + price + '\'' +
							//",description = '" + description + '\'' +
							",id = '" + id + '\'' +
							",stock = '" + stock + '\'' +
							"}";
		}
	}

}
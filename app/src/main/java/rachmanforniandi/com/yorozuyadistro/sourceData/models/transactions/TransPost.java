package rachmanforniandi.com.yorozuyadistro.sourceData.models.transactions;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TransPost{

	@SerializedName("ongkir")
	private int ongkir;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("destination")
	private String destination;

	@SerializedName("grandtotal")
	private int grandtotal;

	@SerializedName("detail")
	private List<DetailItem> detail;

	public void setOngkir(int ongkir){
		this.ongkir = ongkir;
	}

	public int getOngkir(){
		return ongkir;
	}

	public void setUserId(int userId){
		this.userId = userId;
	}

	public int getUserId(){
		return userId;
	}

	public void setDestination(String destination){
		this.destination = destination;
	}

	public String getDestination(){
		return destination;
	}

	public void setGrandtotal(int grandtotal){
		this.grandtotal = grandtotal;
	}

	public int getGrandtotal(){
		return grandtotal;
	}

	public void setDetail(List<DetailItem> detail){
		this.detail = detail;
	}

	public List<DetailItem> getDetail(){
		return detail;
	}


	public class DetailItem{

		@SerializedName("total")
		private int total;

		@SerializedName("price")
		private int price;

		@SerializedName("product_id")
		private int productId;

		@SerializedName("qty")
		private int qty;

		public void setTotal(int total){
			this.total = total;
		}

		public int getTotal(){
			return total;
		}

		public void setPrice(int price){
			this.price = price;
		}

		public int getPrice(){
			return price;
		}

		public void setProductId(int productId){
			this.productId = productId;
		}

		public int getProductId(){
			return productId;
		}

		public void setQty(int qty){
			this.qty = qty;
		}

		public int getQty(){
			return qty;
		}
	}
}
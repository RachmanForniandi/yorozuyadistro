package rachmanforniandi.com.yorozuyadistro.sourceData.models.costs;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Costs {

	@SerializedName("cost")
	private List<Data> cost;

	@SerializedName("service")
	private String service;

	@SerializedName("description")
	private String description;

	public void setCost(List<Data> cost){
		this.cost = cost;
	}

	public List<Data> getCost(){
		return cost;
	}

	public void setService(String service){
		this.service = service;
	}

	public String getService(){
		return service;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}
}